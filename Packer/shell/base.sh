#!/bin/sh

apt-get update
apt-get install -y wget

if ! [ -d /home/vagrant/.ssh ]; then
  mkdir -p /home/vagrant/.ssh
  chown vagrant:vagrant /home/vagrant/.ssh
fi

if ! [ -f /home/vagrant/.ssh/authorized_keys ]; then
  wget --no-check-certificate https://raw.github.com/mitchellh/vagrant/master/keys/vagrant.pub -O /home/vagrant/.ssh/authorized_keys
  chown vagrant:vagrant /home/vagrant/.ssh/authorized_keys
fi

echo "vagrant ALL=(ALL) NOPASSWD: ALL\n" >> /etc/sudoers.d/vagrant


# Apt cleanup.
apt autoremove
apt update

# Zero out the rest of the free space using dd, then delete the written file.
dd if=/dev/zero of=/EMPTY bs=1M
rm -f /EMPTY

# Add `sync` so Packer doesn't quit too early, before the large file is deleted.
#sync